﻿using Pencil.Gaming;
using Pencil.Gaming.MathUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Breakout
{
    class Paddle : Entity
    {
        public Vector2 Position { get; private set; }
        public int Size { get; private set; }
        public Rectangle BarCollision { get { return new Rectangle(Position.X - 5f, Position.Y, Size * sizeStep + 10f, 16f); } }
        public Vector2 CapCollisionLeft { get { return new Vector2(Position.X - 5f, Position.Y + 8f); } }
        public Vector2 CapCollisionRight { get { return new Vector2(Position.X + Size * sizeStep + 5f, Position.Y + 8f); } }
        public bool Sticky { get; set; }

        static readonly float sizeStep = 10f;
        static readonly Sprite spriteCapLeft  = new Sprite(Assets.PaddleCapLeft);
        static readonly Sprite spriteCapRight = new Sprite(Assets.PaddleCapRight);
        static readonly Sprite spriteBar      = new Sprite(Assets.PaddleBar);


        public Paddle()
        {
            Type = EntityType.Paddle;
            IsAlive = true;
            IsCollidable = true;
            IsCollisionStart = false;
            DoRollbackOnCollision = false;
            Position = new Vector2(20, 680);
            Size = 10;
        }
        

        public void ChangeSize(int i)
        {
            var OldSize = Size;
            Size = Util.Clamp(Size + i, 2, 20);
            Position += new Vector2((OldSize - Size) * sizeStep * 0.5f, 0);
        }

        public override bool CheckCollision(Entity other)
        {
            throw new NotImplementedException();
        }

        public override CollisionRecord GetCollision(Entity other)
        {
            throw new NotImplementedException();
        }

        public override void Collide(Entity other, Scene onScene)
        {
            if (other.Type != EntityType.Ball)
            {
                return;
            }
            Ball o = other as Ball;
            var rect = BarCollision;
            
            var n1 = o.Position - Vector2.Clamp(o.Position, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));
            var d1 = n1.LengthSquared;
            var n2 = o.Position - CapCollisionLeft;
            if (n2.LengthSquared < d1)
            {
                return;
            }
            n2 = o.Position - CapCollisionRight;
            if (n2.LengthSquared < d1)
            {
                return;
            }

            n1 = Vector2.Clamp(o.Position, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));

            var pos = 2f * (n1.X - rect.Left) / (Size * sizeStep + 10f) - 1f;
            pos = (float)Math.Asin(pos) * 0.9f + (float)Math.PI / 2f;

            var vx = -(float)Math.Cos(pos);
            var vy = -(float)Math.Sin(pos);

            o.Velocity = new Vector2(vx, vy);
            (onScene as GameScene)?.ChangeBallSpeed(0);
        }

        public override void Render(Renderer r)
        {
            r.AddDrawCall(new DrawCall(spriteCapLeft, Position.X - spriteCapLeft.Width, Position.Y));
            r.AddDrawCall(new DrawCall(spriteBar, Position.X, Position.Y, Size * sizeStep, 1));
            r.AddDrawCall(new DrawCall(spriteCapRight, Position.X + Size * sizeStep, Position.Y));
        }

        public override void Update(float deltaT, InputStore input = null)
        {
            if (input != null)
            {
                var pos = Position;
                pos.X += input.ThisFrame.MouseX - input.PreviousFrame.MouseX;
                pos.X = Util.Clamp(pos.X, spriteCapLeft.Width, 768 - Size * sizeStep - spriteCapRight.Width);
                Position = pos;
            }
        }
    }
}
