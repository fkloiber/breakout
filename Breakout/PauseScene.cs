﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pencil.Gaming;
using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;

namespace Breakout
{
    class PauseScene : Scene
    {
        Scene game;
        Vector2 windowSize;
        List<Entity> entities;

        static Sprite spriteContinue = new Sprite(Assets.TextContinue);
        static Sprite spriteQuitToMenu = new Sprite(Assets.TextQuitToMenu);
        static Sprite spriteQuitToDesktop = new Sprite(Assets.TextQuitToDesktop);

        public PauseScene(ScenePlayer player, Scene g) : base(player)
        {
            game = g;
            int width, height;
            Glfw.GetFramebufferSize(Player.Window, out width, out height);
            windowSize = new Vector2(width, height);
            entities = new List<Entity>();
            float x = 1024f / 2 - Util.ButtonWidth/ 2;
            height = Util.ButtonStartHeight;
            entities.Add(new MenuItem(new Rectangle(x, height, Util.ButtonWidth, Util.ButtonHeight), spriteContinue, Unpause));
            height += Util.ButtonSpacing;
            entities.Add(new MenuItem(new Rectangle(x, height, Util.ButtonWidth, Util.ButtonHeight), spriteQuitToMenu, QuitMenu));
            height += Util.ButtonSpacing;
            entities.Add(new MenuItem(new Rectangle(x, height, Util.ButtonWidth, Util.ButtonHeight), spriteQuitToDesktop, QuitDesktop));
        }

        public override void Render(Renderer r)
        {
            r.PushLevels(100);
            game.Render(r);
            r.PopTransform();
            GL.Clear(ClearBufferMask.DepthBufferBit);
            r.Draw(Util.Black, Vector2.Zero, windowSize, 0.7f, 20);
            foreach (var e in entities)
            {
                e.Render(r);
            }
        }

        public override void Update(float dT, InputStore input)
        {
            foreach (var e in entities)
            {
                e.Update(dT, input);
            }
            if (input.ThisFrame.EscPressed && !input.PreviousFrame.EscPressed)
            {
                Unpause();
            }
        }

        public void QuitMenu()
        {
            Player.ChangeScene(new TransitionScene(Player, this, new MenuScene(Player)));
        }

        public void QuitDesktop()
        {
            Environment.Exit(0);
        }

        public void Unpause()
        {
            Player.ChangeScene(game);
        }
    }
}
