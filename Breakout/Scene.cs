﻿namespace Breakout
{
    abstract class Scene
    {
        public ScenePlayer Player { get; private set; }
        public Scene(ScenePlayer player)
        {
            Player = player;
        }
        public abstract void Update(float dT, InputStore input);
        public abstract void Render(Renderer r);
    }
}
