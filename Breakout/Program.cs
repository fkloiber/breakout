﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pencil.Gaming;
using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;

namespace Breakout
{
    class Program
    {
        static void Main(string[] args)
        {
            Glfw.Init();
            Glfw.WindowHint(WindowHint.ContextVersionMajor, 3);
            Glfw.WindowHint(WindowHint.ContextVersionMinor, 3);
            Glfw.WindowHint(WindowHint.ClientAPI, (int)OpenGLAPI.OpenGLAPI);
            Glfw.WindowHint((WindowHint)WindowAttrib.Resizeable, 0);

            var window = Glfw.CreateWindow(1024, 768, "Breakout", GlfwMonitorPtr.Null, GlfwWindowPtr.Null);
            //Glfw.SetWindowPos(window, 500, 50);
            Glfw.MakeContextCurrent(window);

            var renderer = new Renderer(window);
            var player = new ScenePlayer(window);
            var timer = new FrameTimer();
            var input = new InputStore(window);
            

            Glfw.ShowWindow(window);
            Glfw.SwapInterval(1);
            
            while (!Glfw.WindowShouldClose(window))
            {
                Glfw.PollEvents();

                input.ProcessInput();
                float dt = timer.TimeSinceLastCall();
                /*if (dt > 1)
                {
                    dt = 0.016f;
                }*/
                player.Update(dt, input);
                //player.Update(0.03f, input);
                input.NewFrame();
                player.Render(renderer);

                renderer.Render();
                Glfw.SwapBuffers(window);
            }

            Glfw.DestroyWindow(window);
            Glfw.Terminate();
        }
    }
}
