﻿using System;

namespace Breakout
{
    [Flags]
    enum EntityType
    {
        Invalid   =  0,
        Brick     =  1,
        Ball      =  2,
        Paddle    =  4,
        Collision =  8,
        Item      = 16
    }

    struct CollisionRecord
    {
        public Entity E1, E2;
        public float TimeOfCollision;

        public CollisionRecord(Entity e1, Entity e2, float timeOfCollision)
        {
            E1 = e1;
            E2 = e2;
            TimeOfCollision = timeOfCollision;
        }
    }

    abstract class Entity
    {
        public EntityType Type { get; protected set; }
        public bool IsAlive { get; protected set; }
        public bool IsCollidable { get; protected set; }
        public bool IsCollisionStart { get; protected set; }
        public bool DoRollbackOnCollision { get; protected set; }

        public Entity()
        {
            Type = EntityType.Invalid;
        }

        public abstract void Update(float deltaT, InputStore input = null);

        public abstract bool CheckCollision(Entity other);
        public abstract CollisionRecord GetCollision(Entity other);
        public abstract void Collide(Entity other, Scene onScene);
        public abstract void Render(Renderer r);
    }
}
