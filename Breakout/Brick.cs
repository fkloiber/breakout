﻿using Pencil.Gaming.MathUtils;
using System;

namespace Breakout
{
    class Brick : Entity
    {
        public Vector2 Position { get; private set; }
        public Vector3 Tint { get; private set; }
        public Rectangle Collision { get { return new Rectangle(Position, new Vector2(spriteBrick.Width, spriteBrick.Height)); } }

        static Sprite spriteBrick = new Sprite(Assets.BasicBlock);


        public Brick(Vector2 position, Vector3 tint)
        {
            Type = EntityType.Brick;
            IsAlive = true;
            IsCollidable = true;
            IsCollisionStart = false;
            DoRollbackOnCollision = false;
            Position = position;
            Tint = tint;
        }

        public Brick(float x, float y, Vector3 tint) : this(new Vector2(x, y), tint)
        { }

        public Brick(Vector2 position, float r=1f, float g=1f, float b=1f) : this(position, new Vector3(r, g, b))
        { }

        public Brick(float x, float y, float r=1f, float g=1f, float b=1f) : this(new Vector2(x, y), new Vector3(r, g, b))
        { }


        public override bool CheckCollision(Entity other)
        {
            throw new NotImplementedException();
        }

        public override CollisionRecord GetCollision(Entity other)
        {
            throw new NotImplementedException();
        }

        public override void Collide(Entity other, Scene onScene)
        {
            IsAlive = false;
            var item = ItemProvider.GetItem();
            if (item != null)
            {
                var game = onScene as GameScene;
                item.Position = this.Position + new Vector2(-8, -16);
                game.AddEntity(item);
            }
        }

        public override void Render(Renderer r)
        {
            r.AddDrawCall(new DrawCall(spriteBrick, Position, Tint));
        }

        public override void Update(float deltaT, InputStore input = null)
        {
            return;
        }
    }
}
