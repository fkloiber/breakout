﻿using Pencil.Gaming.MathUtils;
using System;

namespace Breakout
{
    class Collision : Entity
    {
        public Rectangle Rect { get; private set; }
        public Collision(Rectangle rect)
        {
            Type = EntityType.Collision;
            IsAlive = true;
            IsCollidable = true;
            IsCollisionStart = false;
            DoRollbackOnCollision = false;
            Rect = rect;
        }

        public override bool CheckCollision(Entity other)
        {
            throw new NotImplementedException();
        }

        public override void Collide(Entity other, Scene onScene)
        {
            return;
        }

        public override CollisionRecord GetCollision(Entity other)
        {
            throw new NotImplementedException();
        }

        public override void Render(Renderer r)
        {
            return;
        }

        public override void Update(float deltaT, InputStore input = null)
        {
            return;
        }
    }
}
