﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pencil.Gaming.MathUtils;

namespace Breakout
{
    class MenuScene : Scene
    {
        List<Entity> entities;

        Sprite spriteNewGame = new Sprite(Assets.TextNewGame);
        Sprite spriteContinue = new Sprite(Assets.TextContinue);
        Sprite spriteQuit = new Sprite(Assets.TextQuit);

        public MenuScene(ScenePlayer player) : base(player)
        {
            entities = new List<Entity>();
            float x = 1024f / 2 - Util.ButtonWidth/ 2;
            int height = Util.ButtonStartHeight;
            entities.Add(new MenuItem(new Rectangle(x, height, Util.ButtonWidth, Util.ButtonHeight), spriteNewGame, NewGame));
            //height += Util.ButtonSpacing;
            //var c = new MenuItem(new Rectangle(x, height, Util.ButtonWidth, Util.ButtonHeight), spriteContinue, Click);
            height += Util.ButtonSpacing;
            //c.Disabled = true;
            //entities.Add(c);
            entities.Add(new MenuItem(new Rectangle(x, height, Util.ButtonWidth, Util.ButtonHeight), spriteQuit, Quit));
        }

        public override void Render(Renderer r)
        {
            foreach (var e in entities)
            {
                e.Render(r);
            }
        }

        public override void Update(float dT, InputStore input)
        {
            foreach (var e in entities)
            {
                e.Update(dT, input);
            }
        }

        void Quit()
        {
            Environment.Exit(0);
        }

        void NewGame()
        {
            var t = new TransitionScene(Player, this, new GameScene(Player));
            Player.ChangeScene(t);
        }

        void Click()
        {
            Console.WriteLine("Button clicked");
        }
    }
}
