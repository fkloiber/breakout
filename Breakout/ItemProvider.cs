﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pencil.Gaming.MathUtils;
using System.Drawing;

namespace Breakout
{
    class ItemProvider
    {
        static Random rng = new Random();

        public enum ItemType
        {
            FasterBalls, SlowerBalls, WidenPaddle, NarrowPaddle
        }

        public struct ItemInfo
        {
            public Sprite Sprite;
            public Action<Paddle, GameScene> Action;

            public ItemInfo(Bitmap bmp, Action<Paddle, GameScene> action)
            {
                Sprite = new Sprite(bmp);
                Action = action;
            }
        }

        public static readonly ItemInfo[] Items =
        {
            new ItemInfo(Assets.Nothing, Dummy),
            new ItemInfo(Assets.ItemFast, Faster),
            new ItemInfo(Assets.ItemSlow, Slower),
            new ItemInfo(Assets.ItemExpand, Wider),
            new ItemInfo(Assets.ItemContract, Narrower)
        };

        static void Dummy(Paddle paddle, GameScene game)
        { }

        static void Faster(Paddle paddle, GameScene game)
        {
            game.ChangeBallSpeed(+1);
        }

        static void Slower(Paddle paddle, GameScene game)
        {
            game.ChangeBallSpeed(-1);
        }

        static void Wider(Paddle paddle, GameScene game)
        {
            paddle.ChangeSize(+1);
        }

        static void Narrower(Paddle paddle, GameScene game)
        {
            paddle.ChangeSize(-1);
        }

        public static Item GetItem()
        {
            Item Result = null;
            if (rng.NextDouble() < 0.05)
            {
                Result = new Item(rng.Next(1, Items.Length));
                double vx, vy, v, arg;
                v = rng.NextDouble() * 50 + 30;
                arg = rng.NextDouble() * Math.PI * 0.5 - Math.PI * 0.25;
                vx =  v * Math.Sin(arg);
                vy = -v * Math.Cos(arg);
                Result.Velocity = new Vector2((float)vx, (float)vy);
            }

            return Result;
        }
    }
}
