﻿using Pencil.Gaming;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Breakout
{
    class InputStore
    {
        public class InputSet
        {
            public InputSet()
            { }
            public InputSet(InputSet other)
            {
                MouseX = other.MouseX;
                MouseY = other.MouseY;

                MouseLeftPressed = other.MouseLeftPressed;
                MouseRightPressed = other.MouseRightPressed;

                EscPressed = other.EscPressed;
                AddPressed = other.AddPressed;
                SubtractPressed = other.SubtractPressed;

                WindowFocused = other.WindowFocused;
            }

            public float MouseX { get; private set; }
            public float MouseY { get; private set; }

            public bool MouseLeftPressed { get; private set; }
            public bool MouseRightPressed { get; private set; }

            public bool EscPressed { get; private set; }
            public bool AddPressed { get; private set; }
            public bool SubtractPressed { get; private set; }

            public bool WindowFocused { get; private set; }

            public void OnKeyEvent(GlfwWindowPtr window, Key key, int scan, KeyAction action, KeyModifiers mod)
            {
                if ((action & (KeyAction.Press | KeyAction.Release)) != 0)
                {
                    if (key == Key.Escape) { EscPressed = action == KeyAction.Press; }
                    if (key == Key.KPAdd) { AddPressed = action == KeyAction.Press; }
                    if (key == Key.KPSubtract) { SubtractPressed = action == KeyAction.Press; }
                }
            }

            public void OnMouseButtonEvent(GlfwWindowPtr window, MouseButton button, KeyAction action)
            {
                if (action == KeyAction.Press || action == KeyAction.Release)
                {
                    if (button == MouseButton.LeftButton)
                    {
                        MouseLeftPressed = action == KeyAction.Press;
                    } else if(button == MouseButton.RightButton)
                    {
                        MouseRightPressed = action == KeyAction.Press;
                    }
                }
            }

            public void ReadSynchronousInput(GlfwWindowPtr window)
            {
                double mx, my;
                Glfw.GetCursorPos(window, out mx, out my);
                MouseX = (float)mx;
                MouseY = (float)my;

                EscPressed = Glfw.GetKey(window, Key.Escape);
                AddPressed = Glfw.GetKey(window, Key.KPAdd);
                SubtractPressed = Glfw.GetKey(window, Key.KPSubtract);

                MouseLeftPressed = Glfw.GetMouseButton(window, MouseButton.LeftButton);
                MouseRightPressed = Glfw.GetMouseButton(window, MouseButton.RightButton);

                WindowFocused = Glfw.GetWindowAttrib(window, WindowAttrib.Focused) == 1;
            }
        }

        public GlfwWindowPtr Window { get; private set; }
        public InputSet ThisFrame { get; private set; }
        public InputSet PreviousFrame { get; private set; }

        public InputStore(GlfwWindowPtr window)
        {
            Window = window;
            ThisFrame = new InputSet();
            PreviousFrame = ThisFrame;
        }

        public void NewFrame()
        {
            PreviousFrame = ThisFrame;
            ThisFrame = new InputSet(PreviousFrame);
        }

        public void ProcessInput()
        {
            ThisFrame.ReadSynchronousInput(Window);
        }
    }
}
