﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;

namespace Breakout
{
    class ShaderProgram : IDisposable
    {
        uint shaderId;

        static float[] GetMatrixData(Matrix m)
        {
            var Result = new float[16];
            Result[0] = m.M11;
            Result[1] = m.M12;
            Result[2] = m.M13;
            Result[3] = m.M14;
            Result[4] = m.M21;
            Result[5] = m.M22;
            Result[6] = m.M23;
            Result[7] = m.M24;
            Result[8] = m.M31;
            Result[9] = m.M32;
            Result[10] = m.M33;
            Result[11] = m.M34;
            Result[12] = m.M41;
            Result[13] = m.M42;
            Result[14] = m.M43;
            Result[15] = m.M44;
            return Result;
        }

        public ShaderProgram(string vertexShader, string fragmentShader)
        {
            uint vertexId = GL.CreateShader(ShaderType.VertexShader);
            uint fragId = GL.CreateShader(ShaderType.FragmentShader);
            int success;

            GL.ShaderSource(vertexId, vertexShader);
            GL.CompileShader(vertexId);
            GL.GetShader(vertexId, ShaderParameter.CompileStatus, out success);
            if (success == 0)
            {
                Console.WriteLine(GL.GetShaderInfoLog((int)vertexId));
                throw new NotImplementedException();
            }

            GL.ShaderSource(fragId, fragmentShader);
            GL.CompileShader(fragId);
            GL.GetShader(fragId, ShaderParameter.CompileStatus, out success);
            if (success == 0)
            {
                throw new NotImplementedException();
            }

            shaderId = GL.CreateProgram();
            GL.AttachShader(shaderId, vertexId);
            GL.AttachShader(shaderId, fragId);

            GL.LinkProgram(shaderId);
            GL.GetProgram(shaderId, ProgramParameter.LinkStatus, out success);
            if (success == 0)
            {
                throw new NotImplementedException();
            }

            GL.DeleteShader(vertexId);
            GL.DeleteShader(fragId);
        }

        public void Use()
        {
            GL.UseProgram(shaderId);
        }

        public void SetUniform(string name, Matrix mat)
        {
            GL.ProgramUniformMatrix4(shaderId, GL.GetUniformLocation(shaderId, name), 1, false, GetMatrixData(mat));
        }

        public void SetUniform(string name, int val)
        {
            GL.ProgramUniform1(shaderId, GL.GetUniformLocation(shaderId, name), val);
        }

        public void Dispose()
        {
            GL.DeleteProgram(shaderId);
        }
    }
}
