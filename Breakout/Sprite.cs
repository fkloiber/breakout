﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using Pencil.Gaming.Graphics;
using System.Threading.Tasks;

namespace Breakout
{
    class Sprite : IDisposable
    {
        public int TextureId { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        public Sprite(Bitmap bmp, TextureMagFilter filter = TextureMagFilter.Nearest)
        {
            Width = bmp.Width;
            Height = bmp.Height;

            BitmapData bmpData = bmp.LockBits(
                new Rectangle(Point.Empty, bmp.Size),
                ImageLockMode.ReadOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb
            );

            try
            {
                TextureId = GL.GenTexture();
                GL.BindTexture(TextureTarget.Texture2D, TextureId);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)filter);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)filter);
                GL.TexImage2D(
                    TextureTarget.Texture2D,
                    0,
                    PixelInternalFormat.Rgba,
                    bmp.Width,
                    bmp.Height,
                    0,
                    Pencil.Gaming.Graphics.PixelFormat.Bgra,
                    PixelType.UnsignedByte,
                    bmpData.Scan0
                );
                GL.BindTexture(TextureTarget.Texture2D, 0);
            } finally
            {
                bmp.UnlockBits(bmpData);
            }
        }

        public Sprite(int textureId, int width, int height)
        {
            TextureId = textureId;
            Width = width;
            Height = height;
        }

        public void Dispose()
        {
            GL.DeleteTexture(TextureId);
        }
    }
}
