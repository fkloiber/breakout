﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pencil.Gaming.Graphics;
using Pencil.Gaming;

namespace Breakout
{
    class Framebuffer : IDisposable
    {
        int framebufferId, textureId, renderbufferId;
        int width, height;

        public Framebuffer(GlfwWindowPtr window)
        {
            framebufferId = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, framebufferId);

            textureId = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, textureId);

            Glfw.GetFramebufferSize(window, out width, out height);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, width, height, 0, PixelFormat.Rgb, PixelType.UnsignedByte, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.BindTexture(TextureTarget.Texture2D, 0);

            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, textureId, 0);

            renderbufferId = GL.GenRenderbuffer();
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, renderbufferId);
            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.Depth24Stencil8, width, height);
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, 0);

            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthStencilAttachment, RenderbufferTarget.Renderbuffer, renderbufferId);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }

        public void Bind()
        {
            if (textureId == 0)
            {
                throw new NotSupportedException("Cannot bind framebuffer after detatching the texture");
            }
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, framebufferId);
        }

        public void Unbind()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }

        public Sprite GetTexture()
        {
            Sprite ret = new Sprite(textureId, width, height);
            textureId = 0;
            return ret;
        }

        public void Dispose()
        {
            if (textureId != 0)
            {
                GL.DeleteTexture(textureId);
            }
            if (renderbufferId != 0)
            {
                GL.DeleteRenderbuffer(renderbufferId);
            }
            if (framebufferId != 0)
            {
                GL.DeleteFramebuffer(framebufferId);
            }
        }
    }
}
