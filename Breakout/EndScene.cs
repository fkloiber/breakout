﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pencil.Gaming.MathUtils;

namespace Breakout
{
    class EndScene : Scene
    {
        Sprite screen;

        public EndScene(ScenePlayer player, Sprite screen) : base(player)
        {
            this.screen = screen;
        }

        public override void Render(Renderer r)
        {
            r.Draw(screen, Vector2.Zero, Vector2.One, 1f);
        }

        public override void Update(float dT, InputStore input)
        {
            if (input.ThisFrame.MouseLeftPressed && !input.PreviousFrame.MouseLeftPressed)
            {
                Player.ChangeScene(new TransitionScene(Player, this, new MenuScene(Player)));
            }
        }
    }
}
