﻿using System.Diagnostics;

namespace Breakout
{
    class FrameTimer
    {
        Stopwatch stopwatch;
        private long lastCount;
        private float toSeconds;

        public FrameTimer()
        {
            stopwatch = new Stopwatch();
            stopwatch.Start();
            lastCount = stopwatch.ElapsedTicks;
            toSeconds = 1f / Stopwatch.Frequency;
        }
        public float TimeSinceLastCall()
        {
            long currentCount = stopwatch.ElapsedTicks;
            float Result = (float)(currentCount - lastCount) * toSeconds;
            lastCount = currentCount;
            return Result;
        }
    }
}
