﻿using Pencil.Gaming;

namespace Breakout
{
    class ScenePlayer
    {
        public Scene CurrentScene { get; private set; }
        public Scene NextScene { get; private set; }

        public GlfwWindowPtr Window { get; private set; }

        public ScenePlayer(GlfwWindowPtr window)
        {
            //CurrentScene = new GameScene(this);
            CurrentScene = new MenuScene(this);
            NextScene = CurrentScene;
            Window = window;
        }

        public void Update(float dT, InputStore input)
        {
            if (!ReferenceEquals(CurrentScene, NextScene))
            {
                CurrentScene = NextScene;
            }
            CurrentScene.Update(dT, input);
        }

        public void Render(Renderer r)
        {
            CurrentScene.Render(r);
        }

        public void ChangeScene(Scene s)
        {
            NextScene = s;
        }
    }
}
