﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pencil.Gaming.MathUtils;

namespace Breakout
{
    class Item : Entity
    {
        public Vector2 Position, Velocity;
        int ItemType;

        public Item(int type)
        {
            Type = EntityType.Item;
            ItemType = type;
            IsAlive = true;
            IsCollidable = true;
            IsCollisionStart = true;
            DoRollbackOnCollision = false;
        }

        public override bool CheckCollision(Entity other)
        {
            if (other.Type != EntityType.Paddle || ItemType == 0)
            {
                return false;
            }

            Paddle o = other as Paddle;
            var rect2 = o.BarCollision;

            /*
            var rect1 = { x: 5, y: 5, width: 50, height: 50}
            var rect2 = { x: 20, y: 10, width: 10, height: 10 }

            if (rect1.x<rect2.x + rect2.width &&
               rect1.x + rect1.width> rect2.x &&
               rect1.y<rect2.y + rect2.height &&
               rect1.height + rect1.y> rect2.y)
            */
            var rect1 = new Rectangle(Position, 48, 48);

            if (rect1.Left < rect2.Right
             && rect1.Right > rect2.Left
             && rect1.Top < rect2.Bottom
             && rect1.Bottom > rect2.Top)
            {
                return true;
            }

            return false;
        }

        public override void Collide(Entity other, Scene onScene)
        {
            var p = other as Paddle;
            var g = onScene as GameScene;

            ItemProvider.Items[ItemType].Action(p, g);
            ItemType = 0;
            IsAlive = false;
        }

        public override CollisionRecord GetCollision(Entity other)
        {
            CollisionRecord record = new CollisionRecord();

            record.E1 = this;
            record.E2 = other;
            record.TimeOfCollision = float.NegativeInfinity;

            return record;
        }

        public override void Render(Renderer r)
        {
            if (ItemType != 0)
            {
                r.Draw(ItemProvider.Items[ItemType].Sprite, Position, Vector2.One, 1f, 8);
            }
        }

        public override void Update(float deltaT, InputStore input = null)
        {
            if (input != null)
            {
                Velocity += deltaT * Util.Gravity;
                Position += deltaT * Velocity;
            }
        }
    }
}
