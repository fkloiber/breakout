﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pencil.Gaming.MathUtils;

namespace Breakout
{
    class Util
    {
        public static readonly Sprite Black = new Sprite(Assets.black);
        public static readonly Sprite Congratulations = new Sprite(Assets.Congratulations);
        public static readonly Sprite GameOver = new Sprite(Assets.GameOver);
        public static readonly int ButtonWidth = 170, ButtonHeight = 50, ButtonSpacing = 60, ButtonStartHeight = 500;
        public static readonly Vector2 Gravity = new Vector2(0, 40);
        //public static readonly Vector2 Gravity = new Vector2(0, 0);
        public const float TransitionTime = 0.3f;
        public static T Clamp<T>(T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0)
                return min;
            if (val.CompareTo(max) > 0)
                return max;
            return val;
        }
    }
}
