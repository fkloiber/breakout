﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using Pencil.Gaming.MathUtils;

namespace Breakout
{
    class MenuItem : Entity
    {
        struct ButtonSprites
        {
            public Sprite CornerTL, CornerTR, CornerBL, CornerBR;
            public Sprite EdgeT, EdgeB, EdgeL, EdgeR, Mid;

            public ButtonSprites(Bitmap ctl, Bitmap ctr, Bitmap cbl, Bitmap cbr, Bitmap et, Bitmap eb, Bitmap el, Bitmap er, Bitmap m)
            {
                CornerTL = new Sprite(ctl);
                CornerTR = new Sprite(ctr);
                CornerBL = new Sprite(cbl);
                CornerBR = new Sprite(cbr);
                EdgeT = new Sprite(et);
                EdgeB = new Sprite(eb);
                EdgeL = new Sprite(el);
                EdgeR = new Sprite(er);
                Mid = new Sprite(m);
            }
        }

        static ButtonSprites button = new ButtonSprites(Assets.ButtonTL, Assets.ButtonTR, Assets.ButtonBL, Assets.ButtonBR, Assets.ButtonT, Assets.ButtonB, Assets.ButtonL, Assets.ButtonR, Assets.ButtonM);
        static ButtonSprites buttonHover = new ButtonSprites(Assets.ButtonHoverTL, Assets.ButtonHoverTR, Assets.ButtonHoverBL, Assets.ButtonHoverBR, Assets.ButtonHoverT, Assets.ButtonHoverB, Assets.ButtonHoverL, Assets.ButtonHoverR, Assets.ButtonHoverM);
        static ButtonSprites buttonPress = new ButtonSprites(Assets.ButtonPressedTL, Assets.ButtonPressedTR, Assets.ButtonPressedBL, Assets.ButtonPressedBR, Assets.ButtonPressedT, Assets.ButtonPressedB, Assets.ButtonPressedL, Assets.ButtonPressedR, Assets.ButtonPressedM);
        static int z = 2;

        Pencil.Gaming.MathUtils.Rectangle rect;
        Vector2 textPos;
        Sprite text;
        bool hovered, pressed;
        Action action;

        public bool Disabled { get; set; }

        public MenuItem(Pencil.Gaming.MathUtils.Rectangle r, Sprite t, Action a)
        {
            IsAlive = true;
            IsCollidable = false;
            IsCollisionStart = false;
            DoRollbackOnCollision = false;
            rect = r;
            text = t;
            action = a;
            textPos = new Vector2((float)Math.Floor(rect.Left + 0.5f * (rect.Width - text.Width)),
                                  (float)Math.Floor(rect.Top + 0.5f * (rect.Height - text.Height)));
        }

        public override bool CheckCollision(Entity other)
        {
            throw new NotImplementedException();
        }

        public override void Collide(Entity other, Scene onScene)
        {
            throw new NotImplementedException();
        }

        public override CollisionRecord GetCollision(Entity other)
        {
            throw new NotImplementedException();
        }

        void RenderButton(Renderer r, ButtonSprites b)
        {
            r.AddDrawCall(new DrawCall(b.CornerTL, rect.Left, rect.Top, z, z));
            r.AddDrawCall(new DrawCall(b.CornerTR, rect.Right - z * b.CornerTR.Width, rect.Top, z, z));
            r.AddDrawCall(new DrawCall(b.CornerBL, rect.Left, rect.Bottom - z * b.CornerBL.Height, z, z));
            r.AddDrawCall(new DrawCall(b.CornerBR, rect.Right - z * b.CornerBR.Width, rect.Bottom - z * b.CornerBL.Height, z, z));

            r.AddDrawCall(new DrawCall(b.EdgeT, rect.Left, rect.Top, rect.Width, z, 11));
            r.AddDrawCall(new DrawCall(b.EdgeB, rect.Left, rect.Bottom - z * b.EdgeB.Height, rect.Width, z, 11));
            r.AddDrawCall(new DrawCall(b.EdgeL, rect.Left, rect.Top, z, rect.Height, 11));
            r.AddDrawCall(new DrawCall(b.EdgeR, rect.Right - z * b.EdgeR.Width, rect.Top, z, rect.Height, 11));

            r.AddDrawCall(new DrawCall(b.Mid, rect.Left, rect.Top, rect.Width, rect.Height, 12));
        }

        public override void Render(Renderer r)
        {
            RenderButton(r, pressed ? buttonPress: (hovered ? buttonHover : button));
            r.AddDrawCall(new DrawCall(text, textPos, Disabled ? 0.7f * Vector3.One: Vector3.Zero, 9));
        }

        public override void Update(float deltaT, InputStore input = null)
        {
            if (Disabled)
            {
                hovered = false;
                pressed = false;
                return;
            }
            bool activate = false;
            Vector2 mousePos = new Vector2(input.ThisFrame.MouseX, input.ThisFrame.MouseY);
            hovered = rect.IsVectorEnclosedBy(mousePos);
            activate = input.PreviousFrame.MouseLeftPressed && !input.ThisFrame.MouseLeftPressed && pressed;
            pressed = hovered && input.ThisFrame.MouseLeftPressed && (pressed || !input.PreviousFrame.MouseLeftPressed);

            if (activate)
            {
                action();
            }
        }
    }
}
