﻿using Pencil.Gaming.MathUtils;
using Pencil.Gaming;

namespace Breakout
{
    class TransitionScene : Scene
    {
        Scene OldScene, NewScene;
        Vector2 windowSize;
        float transitionTime, timePassed;
        float htt;

        public TransitionScene(ScenePlayer player, Scene oldScene, Scene newScene, float time = Util.TransitionTime) : base(player)
        {
            OldScene = oldScene;
            NewScene = newScene;
            int width, height;
            Glfw.GetFramebufferSize(Player.Window, out width, out height);
            windowSize = new Vector2(width, height);
            transitionTime = time;
            htt = time * 0.5f;
            timePassed = 0f;
        }

        public override void Render(Renderer r)
        {
            if (timePassed > 0.5f * transitionTime)
            {
                r.PushLevels(100);
                NewScene.Render(r);
                r.PopTransform();
                r.Draw(Util.Black, Vector2.Zero, windowSize, 1f - (timePassed - htt) / htt);
            }
            else
            {
                r.PushLevels(100);
                OldScene.Render(r);
                r.PopTransform();
                r.Draw(Util.Black, Vector2.Zero, windowSize, timePassed / htt);
            }
        }

        public override void Update(float dT, InputStore intput)
        {
            timePassed += dT;
            Glfw.SetInputMode(Player.Window, InputMode.CursorMode, CursorMode.CursorNormal);
            if (transitionTime <= timePassed)
            {
                Player.ChangeScene(NewScene);
            }
        }
    }
}
