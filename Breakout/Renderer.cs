﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pencil.Gaming.MathUtils;
using Pencil.Gaming;
using Pencil.Gaming.Graphics;

namespace Breakout
{
    struct DrawCall
    {
        public Sprite sprite;
        public Matrix modelTransform;
        public Matrix colorTransform;

        public DrawCall(Sprite _sprite, float x, float y, int lvl = 10)
        {
            sprite = _sprite;
            modelTransform = Matrix.CreateScale(_sprite.Width, _sprite.Height, 1.0f) * Matrix.CreateTranslation(x, y, -lvl);
            colorTransform = Matrix.Identity;
        }

        public DrawCall(Sprite _sprite, Vector2 pos, int lvl = 10)
        {
            sprite = _sprite;
            modelTransform = Matrix.CreateScale(_sprite.Width, _sprite.Height, 1.0f) * Matrix.CreateTranslation(pos.X, pos.Y, -lvl);
            colorTransform = Matrix.Identity;
        }

        public DrawCall(Sprite _sprite, float x, float y, Vector3 tint, int lvl = 10)
        {
            sprite = _sprite;
            modelTransform = Matrix.CreateScale(_sprite.Width, _sprite.Height, 1.0f) * Matrix.CreateTranslation(x, y, -lvl);
            colorTransform = Matrix.CreateScale(tint);
        }

        public DrawCall(Sprite _sprite, Vector2 pos, Vector3 tint, int lvl = 10)
        {
            sprite = _sprite;
            modelTransform = Matrix.CreateScale(_sprite.Width, _sprite.Height, 1.0f) * Matrix.CreateTranslation(pos.X, pos.Y, -lvl);
            colorTransform = Matrix.CreateScale(tint);
        }

        public DrawCall(Sprite _sprite, float x, float y, float stretchX, float stretchY, int lvl = 10)
        {
            sprite = _sprite;
            modelTransform = Matrix.CreateScale(_sprite.Width * stretchX, _sprite.Height * stretchY, 1.0f) * Matrix.CreateTranslation(x, y, -lvl);
            colorTransform = Matrix.Identity;
        }

        public DrawCall(Sprite _sprite, Matrix transform, Vector3 tint)
        {
            sprite = _sprite;
            modelTransform = transform;
            colorTransform = Matrix.CreateScale(tint);
        }

        public DrawCall(Sprite _sprite, Vector2 pos, float alpha, int lvl = 10)
        {
            sprite = _sprite;
            modelTransform = Matrix.CreateScale(_sprite.Width, _sprite.Height, 1f) * Matrix.CreateTranslation(pos.X, pos.Y, -lvl);
            colorTransform = Matrix.Identity;
            colorTransform.M44 = alpha;
        }
    }

    class Renderer
    {

        List<DrawCall> commands;
        Stack<Matrix> transforms;
        GlfwWindowPtr targetWindow;
        ShaderProgram shader;
        int vertexArray;

        public Renderer(GlfwWindowPtr window)
        {
            commands = new List<DrawCall>();
            transforms = new Stack<Matrix>();
            transforms.Push(Matrix.Identity);
            targetWindow = window;
            Setup();
        }

        private void Setup()
        {
            Glfw.MakeContextCurrent(targetWindow);
            //GL.ClearColor(0.3F, 0.5F, 0.3F, 1.0F);
            GL.ClearColor(0F, 0F, 0F, 1.0F);

            shader = new ShaderProgram(Assets.shader_vert, Assets.shader_frag);
            vertexArray = GL.GenVertexArray();
            int buffer = GL.GenBuffer();
            float[] vertices = { 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f };

            GL.BindVertexArray(vertexArray);
            GL.BindBuffer(BufferTarget.ArrayBuffer, buffer);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * 4), vertices, BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, 2 * 4, 0);
            GL.EnableVertexAttribArray(0);
            GL.BindVertexArray(0);
        }

        public void NewFrame()
        {
            commands.Clear();
        }

        public void PushTranslation(float x, float y)
        {
            transforms.Push(Matrix.CreateTranslation(x, y, 0f) * transforms.Peek());
        }

        public void PushLevels(int lvl)
        {
            transforms.Push(Matrix.CreateTranslation(0f, 0f, -lvl) * transforms.Peek());
        }

        public void PopTransform()
        {
            transforms.Pop();
        }

        public void AddDrawCall(DrawCall call)
        {
            call.modelTransform = call.modelTransform * transforms.Peek();
            commands.Add(call);
        }

        public void Render()
        {
            Glfw.MakeContextCurrent(targetWindow);

            int WindowWidth, WindowHeight;
            Glfw.GetFramebufferSize(targetWindow, out WindowWidth, out WindowHeight);
            GL.Viewport(0, 0, WindowWidth, WindowHeight);

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Less);

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            
            shader.Use();
            shader.SetUniform("projection", Matrix.CreateOrthographicOffCenter(0.0f, 1024.0f, 768.0f, 0.0f, -0.1f, 1000.0f));
            GL.BindVertexArray(vertexArray);

            var calls = commands.OrderBy(c => c.modelTransform.M43).ToList();

            foreach (var call in calls)
            {
                GL.BindTexture(TextureTarget.Texture2D, call.sprite.TextureId);
                shader.SetUniform("sprite", 0);
                shader.SetUniform("model", call.modelTransform);
                shader.SetUniform("colorTransform", call.colorTransform);
                GL.DrawArrays(BeginMode.TriangleFan, 0, 4);
            }
            
            GL.BindVertexArray(0);
            NewFrame();
        }

        public void Draw(Sprite sprite, Vector2 pos, Vector2 stretch, float alpha, int lvl = 10)
        {
            var call = new DrawCall();
            call.sprite = sprite;
            call.modelTransform = Matrix.CreateScale(stretch.X * sprite.Width, stretch.Y * sprite.Height, 1f) * Matrix.CreateTranslation(pos.X, pos.Y, -lvl) * transforms.Peek();
            call.colorTransform = Matrix.Identity;
            call.colorTransform.M44 = alpha;
            commands.Add(call);
        }
    }
}
