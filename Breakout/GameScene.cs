﻿using Pencil.Gaming;
using Pencil.Gaming.MathUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Breakout
{
    class GameScene : Scene
    {
        Sprite borderSprite, backgroundSprite;
        List<Entity> entities;

        int bricksToDestroy;
        int ballsInGame;

        int ballSpeedi;
        float ballSpeed;

        bool hasFocus;

        public GameScene(ScenePlayer player) : base(player)
        {
            borderSprite = new Sprite(Assets.Border);
            backgroundSprite = new Sprite(Assets.Background);
            entities = new List<Entity>();

            bricksToDestroy = 0;
            ballsInGame = 0;

            var e = new Paddle();
            //e.Sticky = true;
            AddEntity(e);
            AddEntity(new Collision(new Rectangle(-100, -100, 100, 1000)));
            AddEntity(new Collision(new Rectangle(768, -100, 100, 1000)));
            AddEntity(new Collision(new Rectangle(0, -100, 768, 100)));
            
            for (int y = 0; y < 20; ++y)
            {
                for (int x = 0; x < 22; ++x)
                {
                    AddEntity(new Brick(x * 32 + 32, y * 16 + 32, x / 22f, y / 20f));
                }
            }
            AddEntity(new Ball(200, 600, 200, 10, -20));

            ballSpeedi = 0;
            ChangeBallSpeed(8);

            hasFocus = false;
        }

        public void AddEntity(Entity e)
        {
            entities.Add(e);
            switch (e.Type)
            {
                case EntityType.Ball:  ballsInGame++; break;
                case EntityType.Brick: bricksToDestroy++; break;
            }
        }

        public void ChangeBallSpeed(int i)
        {
            ballSpeedi = Util.Clamp(ballSpeedi + i, 3, 25);
            ballSpeed = ballSpeedi * 30f;

            foreach (var e in entities)
            {
                (e as Ball)?.AdjustSpeed(ballSpeed);
            }
        }

        public void UnstickBalls(Paddle p)
        {
            foreach (var e in entities)
            {
                if (e is Ball)
                {
                    var b = e as Ball;
                    if (b.StuckTo == p)
                    {
                        b.Unstick();
                    }
                }
            }
        }

        public override void Render(Renderer r)
        {
            r.AddDrawCall(new DrawCall(borderSprite, 0, 0, 5));
            r.AddDrawCall(new DrawCall(backgroundSprite, 0, 0, 99));

            r.PushTranslation(24, 24);
            {
                foreach (var e in entities)
                {
                    e.Render(r);
                }
            }
            r.PopTransform();
        }

        private bool ResolveCollisions()
        {
            List<CollisionRecord> collisions = new List<CollisionRecord>();
            for (int i = 0; i < entities.Count; ++i)
            {
                if (!entities[i].IsAlive || !entities[i].IsCollidable)
                {
                    continue;
                }
                for (int j = i + 1; j < entities.Count; ++j)
                {
                    if (!entities[j].IsAlive || !entities[j].IsCollidable || !entities[j].IsCollisionStart)
                    {
                        continue;
                    }
                    if (entities[j].CheckCollision(entities[i]))
                    {
                        collisions.Add(entities[j].GetCollision(entities[i]));
                    }
                }
            }
            if (collisions.Count == 0)
            {
                return false;
            }

            collisions.Sort((c1, c2) => c1.TimeOfCollision.CompareTo(c2.TimeOfCollision));

            CollisionRecord collision = collisions[0];
            Debug.Assert(collision.TimeOfCollision < 0);

            if (collision.E1.DoRollbackOnCollision || collision.E2.DoRollbackOnCollision)
            {
                foreach (var e in entities)
                {
                    e.Update(collision.TimeOfCollision);
                }
            }

            collision.E1.Collide(collision.E2, this);
            collision.E2.Collide(collision.E1, this);

            if (collision.E1.DoRollbackOnCollision || collision.E2.DoRollbackOnCollision)
            {
                foreach (var e in entities)
                {
                    e.Update(-collision.TimeOfCollision);
                }
            }

            return true;
        }

        public override void Update(float dT, InputStore input)
        {
            if (input.ThisFrame.WindowFocused && !hasFocus)
            {
                Glfw.SetInputMode(input.Window, InputMode.CursorMode, CursorMode.CursorCaptured);
                hasFocus = true;
            } else if (!input.ThisFrame.WindowFocused && hasFocus)
            {
                Pause();
            }
            if (input.ThisFrame.EscPressed && !input.PreviousFrame.EscPressed)
            {
                Pause();
            }
            foreach (var e in entities)
            {
                e.Update(dT, input);
            }

            while (ResolveCollisions()) ;

            entities = entities.Where((e) =>
            {
                if (!e.IsAlive)
                {
                    switch (e.Type)
                    {
                        case EntityType.Ball: ballsInGame--; break;
                        case EntityType.Brick: bricksToDestroy--; break;
                    }
                }
                return e.IsAlive;
            }
            ).ToList();

            if (ballsInGame == 0)
            {
                Player.ChangeScene(new TransitionScene(Player, this, new EndScene(Player, Util.GameOver)));
            }

            if (bricksToDestroy == 0)
            {
                Player.ChangeScene(new TransitionScene(Player, this, new EndScene(Player, Util.Congratulations)));
            }
        }

        void Pause()
        {
            Glfw.SetInputMode(Player.Window, InputMode.CursorMode, CursorMode.CursorNormal);
            hasFocus = false;
            Player.ChangeScene(new PauseScene(Player, this));
        }

        public static bool IsCollidingCircCirc(Vector2 p1, float r1, Vector2 p2, float r2)
        {
            var dist = p1 - p2;
            var r = r1 + r2;
            return dist.LengthSquared < r * r;
        }

        public static bool IsCollidingCircRect(Vector2 pos, float rad, Rectangle rect)
        {
            var nearest = Vector2.Clamp(pos, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));
            var dist = pos - nearest;
            return dist.LengthSquared < rad * rad;
        }

        public static bool IsCollidingRectRect(Vector2 p1, Vector2 s1, Vector2 p2, Vector2 s2)
        {
            return false;
        }
    }
}
