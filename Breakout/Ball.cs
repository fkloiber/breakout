﻿using Pencil.Gaming.MathUtils;
using System;

namespace Breakout
{
    class Ball : Entity
    {
        public Vector2 Position { get; private set; }
        public Vector2 Velocity { get; set; }
        public Paddle StuckTo { get; private set; }

        Vector2 stuckOffset;

        static Sprite spriteBrick = new Sprite(Assets.BasicBall);

        public Ball(Vector2 position, float s, Vector2 dir)
        {
            Type = EntityType.Ball;
            IsAlive = true;
            IsCollidable = true;
            IsCollisionStart = true;
            DoRollbackOnCollision = true;
            Position = position;
            Velocity = dir;
            StuckTo = null;
        }

        public Ball(float x, float y, float s, Vector2 dir) : this(new Vector2(x, y), s, dir)
        { }

        public Ball(float x, float y, float s, float dx, float dy) : this(new Vector2(x, y), s, new Vector2(dx, dy))
        { }

        public Ball(Vector2 position, float s, float dx, float dy) : this(position, s, new Vector2(dx, dy))
        { }


        public void AdjustSpeed(float s)
        {
            var v = Velocity;
            v.Normalize();
            Velocity = s * v;
        }

        public override bool CheckCollision(Entity other)
        {
            if ((other.Type & (EntityType.Ball | EntityType.Brick | EntityType.Collision | EntityType.Paddle)) == 0)
            {
                return false;
            }

            switch(other.Type)
            {
                case EntityType.Ball:
                    {
                        Ball o = other as Ball;
                        return GameScene.IsCollidingCircCirc(Position, 6, o.Position, 6);
                    }
                case EntityType.Brick:
                    {
                        Brick o = other as Brick;
                        return GameScene.IsCollidingCircRect(Position, 6, o.Collision);
                    }
                case EntityType.Collision:
                    {
                        Collision o = other as Collision;
                        return GameScene.IsCollidingCircRect(Position, 6, o.Rect);
                    }
                case EntityType.Paddle:
                    {
                        Paddle o = other as Paddle;
                        return GameScene.IsCollidingCircRect(Position, 6, o.BarCollision)
                            || GameScene.IsCollidingCircCirc(Position, 6, o.CapCollisionLeft,  8)
                            || GameScene.IsCollidingCircCirc(Position, 6, o.CapCollisionRight, 8);
                    }
            }

            return false;
        }

        public override void Collide(Entity other, Scene onScene)
        {
            if ((other.Type & (EntityType.Ball | EntityType.Brick | EntityType.Collision | EntityType.Paddle)) == 0)
            {
                throw new NotImplementedException();
            }

            switch (other.Type)
            {
                case EntityType.Ball:
                    {
                        Ball o = other as Ball;
                        var dp = Position - o.Position;
                        dp.Normalize();

                          Velocity -= 2 * Vector2.Dot(  Velocity, dp) * dp;
                        o.Velocity -= 2 * Vector2.Dot(o.Velocity, dp) * dp;
                    }
                    break;

                case EntityType.Brick:
                    {
                        Brick o = other as Brick;
                        var rect = o.Collision;
                        var nearest = Vector2.Clamp(Position, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));
                        var dp = Position - nearest;
                        dp.Normalize();

                        Velocity -= 2 * Vector2.Dot(Velocity, dp) * dp;
                    }
                    break;

                case EntityType.Collision:
                    {
                        Collision o = other as Collision;
                        var rect = o.Rect;
                        var nearest = Vector2.Clamp(Position, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));
                        var dp = Position - nearest;
                        dp.Normalize();

                        Velocity -= 2 * Vector2.Dot(Velocity, dp) * dp;
                    }
                    break;

                case EntityType.Paddle:
                    {
                        Paddle o = other as Paddle;
                        var rect = o.BarCollision;
                        var n1 = Position - Vector2.Clamp(Position, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));
                        var d1 = n1.LengthSquared;
                        var n2 = Position - o.CapCollisionLeft;

                        if (n2.LengthSquared < d1)
                        {
                            n1 = n2;
                            d1 = n2.LengthSquared;
                        }

                        var n3 = Position - o.CapCollisionRight;

                        if (n3.LengthSquared < d1)
                        {
                            n1 = n3;
                        }

                        var dp = n1;
                        dp.Normalize();
                        if (Vector2.Dot(Velocity, dp) < 0)
                        {
                            Velocity -= 2 * Vector2.Dot(Velocity, dp) * dp;
                        }

                        if (o.Sticky)
                        {
                            StuckTo = o;
                            stuckOffset = Position - o.Position;
                            Velocity = new Vector2(0, 1);
                        }
                    }
                    break;
            }
        }

        private bool EdgeTime(Vector2 pos, Vector2 vel, float r, Vector2 point, ref float time)
        {
            var dist = pos - point;
            var dp2 = dist.LengthSquared;
            var dpv = Vector2.Dot(dist, vel);
            var v2 = vel.LengthSquared;
            var r2 = r * r;

            var det = dpv * dpv - v2 * (dp2 - r2);
            if (det < 0)
            {
                return false;
            }

            det = (float)Math.Sqrt(det);
            time = Math.Min((-dpv + det) / v2, (-dpv - det) / v2);
            return true;
        }

        private void CollisionTimeBallRect(Vector2 pos, Vector2 vel, float r, Rectangle rect, ref CollisionRecord coll)
        {
            float minTime = float.PositiveInfinity;
            Vector2 posAtTime, nearAtTime;
            float clearTime;

            // clear top
            clearTime = (rect.Top - pos.Y - r) / vel.Y;
            posAtTime = pos + vel * clearTime;
            nearAtTime = Vector2.Clamp(posAtTime, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));
            if (Math.Abs((posAtTime - nearAtTime).LengthSquared - r * r) < 0.001f
             && clearTime< minTime)
            {
                minTime = clearTime;
            }
            
            // clear bottom
            clearTime = (rect.Bottom - pos.Y + r) / vel.Y;
            posAtTime = pos + vel * clearTime;
            nearAtTime = Vector2.Clamp(posAtTime, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));
            if (Math.Abs((posAtTime - nearAtTime).LengthSquared - r * r) < 0.001f
             && clearTime < minTime)
            {
                minTime = clearTime;
            }

            // clear left
            clearTime = (rect.Left - pos.X - r) / vel.X;
            posAtTime = pos + vel * clearTime;
            nearAtTime = Vector2.Clamp(posAtTime, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));
            if (Math.Abs((posAtTime - nearAtTime).LengthSquared - r * r) < 0.001f
             && clearTime < minTime)
            {
                minTime = clearTime;
            }

            // clear right
            clearTime = (rect.Right - pos.X + r) / vel.X;
            posAtTime = pos + vel * clearTime;
            nearAtTime = Vector2.Clamp(posAtTime, new Vector2(rect.Left, rect.Top), new Vector2(rect.Right, rect.Bottom));
            if (Math.Abs((posAtTime - nearAtTime).LengthSquared - r * r) < 0.001f
             && clearTime < minTime)
            {
                minTime = clearTime;
            }

            if (EdgeTime(pos, vel, r, new Vector2(rect.Left, rect.Top), ref clearTime)
             && clearTime < minTime)
            {
                minTime = clearTime;
            }

            if (EdgeTime(pos, vel, r, new Vector2(rect.Right, rect.Top), ref clearTime)
             && clearTime < minTime)
            {
                minTime = clearTime;
            }

            if (EdgeTime(pos, vel, r, new Vector2(rect.Left, rect.Bottom), ref clearTime)
             && clearTime < minTime)
            {
                minTime = clearTime;
            }

            if (EdgeTime(pos, vel, r, new Vector2(rect.Right, rect.Bottom), ref clearTime)
             && clearTime < minTime)
            {
                minTime = clearTime;
            }

            coll.TimeOfCollision = minTime;
        }

        public override CollisionRecord GetCollision(Entity other)
        {
            if ((other.Type & (EntityType.Ball | EntityType.Brick | EntityType.Collision | EntityType.Paddle)) == 0)
            {
                throw new NotImplementedException();
            }

            CollisionRecord record = new CollisionRecord();
            record.E1 = this;
            record.E2 = other;

            switch (other.Type)
            {
                case EntityType.Ball:
                    {
                        Ball o = other as Ball;
                        record.TimeOfCollision = (12 - (Position - o.Position).Length) / (Velocity - o.Velocity).Length;
                    }
                    break;

                case EntityType.Brick:
                    {
                        Brick o = other as Brick;
                        CollisionTimeBallRect(Position, Velocity, 6f, o.Collision, ref record);
                    }
                    break;

                case EntityType.Collision:
                    {
                        Collision o = other as Collision;
                        CollisionTimeBallRect(Position, Velocity, 6f, o.Rect, ref record);
                    }
                    break;

                case EntityType.Paddle:
                    {
                        Paddle o = other as Paddle;
                        CollisionTimeBallRect(Position, Velocity, 6f, o.BarCollision, ref record);

                        float clearTime = 0;
                        if (EdgeTime(Position, Velocity, 14f, o.CapCollisionLeft, ref clearTime)
                         && clearTime < record.TimeOfCollision)
                        {
                            record.TimeOfCollision = clearTime;
                        }

                        if (EdgeTime(Position, Velocity, 14f, o.CapCollisionRight, ref clearTime)
                         && clearTime < record.TimeOfCollision)
                        {
                            record.TimeOfCollision = clearTime;
                        }
                    }
                    break;
            }

            return record;
        }

        public override void Render(Renderer r)
        {
            r.AddDrawCall(new DrawCall(spriteBrick, Position - new Vector2(6, 6)));
        }

        public void Unstick()
        {
            StuckTo = null;
        }

        public override void Update(float deltaT, InputStore input = null)
        {
            if (StuckTo == null)
            {
                Position += deltaT * Velocity;
                if (Position.Y > 750)
                {
                    IsAlive = false;
                }
            } else
            {
                Position = StuckTo.Position + stuckOffset;
            }
        }
    }
}
